function wpBase64(input: string) {
  const phpassbase64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  const res = [0];
  let currentBit = 0, index = 0;
  input.split('').forEach(l => {
      const i = phpassbase64.indexOf(l);
      res[index] += (i << currentBit) & 0xff;
      currentBit += 6;
      if(currentBit >= 8) {
          currentBit -= 8;
          index++;
          res.push(i >> (6-currentBit));
      }
  });
  if(res[res.length - 1] === 0) {
    res.pop();
  }

  return Buffer.from(res);
}

export type WpHash = {
  key: string,
  salt: string,
}

export const WpHashType = "MD5";

export const WpHashRounds = 8192;

export function parseWpHash(rawWpHash: string): WpHash {
  const salt = rawWpHash.substring(4, 12);
  const key = wpBase64(rawWpHash.slice(12)).toString('hex');
  return { salt, key };
}
