#!/usr/bin/env node

import { initializeApp, cert } from "firebase-admin/app";
import { getAuth } from "firebase-admin/auth"

import { ArgumentParser } from "argparse";

import * as fs from 'fs';
import { parseWpHash, WpHashRounds, WpHashType } from "./wpHash";

// Parse arguments.

const parser = new ArgumentParser({
  description: 'Argparse example'
});

parser.add_argument('-c', '--cert', {
  help: 'Path to ServiceAccount certificate file.',
  required: true,
});
parser.add_argument('input', {
  help: 'Path to JSON file containing Wordpress users.',
});

const args = parser.parse_args();
const saCertPath: string = args.cert;
const inputPath: string = args.input;

// Load data.

const saCert: any = JSON.parse(fs.readFileSync(saCertPath, 'utf8'));
const wpUsers: any = JSON.parse(fs.readFileSync(inputPath, 'utf8'));

// Convert Wordpress users to Firebase format.

type User = {
  uid: string,
  email: string,
  passwordHash: Buffer,
  passwordSalt: Buffer,
  displayName: string,
}

let users: User[] = [];
for (let row of wpUsers) {
  let wp_ID: number = row.ID;
  if (typeof wp_ID !== "number") {
    throw new Error("ID field is required and must be number.");
  }
  let wp_userLogin: string = row.user_login;
  if (typeof wp_userLogin !== "string") {
    throw new Error("user_login field is required and must be string.");
  }
  let wp_userPass: string = row.user_pass;
  if (typeof wp_userPass !== "string") {
    throw new Error("user_pass field is required and must be string.");
  }
  let wp_userEmail: string = row.user_email;
  if (typeof wp_userEmail !== "string") {
    throw new Error("user_email field is required and must be string.");
  }
  let wp_displayName: string = row.display_name;
  if (typeof wp_displayName !== "string") {
    throw new Error("display_name field is required and must be string.");
  }

  const wpHash = parseWpHash(wp_userPass);

  users.push({
    uid: wp_ID.toString(),
    email: wp_userEmail,
    passwordHash: Buffer.from(wpHash.key),
    passwordSalt: Buffer.from(wpHash.salt),
    displayName: wp_displayName,
  });
}

// Authenticate in Firebase.

initializeApp({
  credential: cert(saCert)
});
let auth = getAuth();

// Import users into Firebase.

auth.importUsers(users, { hash: {
  algorithm: WpHashType,
  rounds: WpHashRounds
}});
console.log("Users imported successfully!");
